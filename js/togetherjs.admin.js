(function($) {

    /**
     * Provide the summary information for the tracking settings vertical tabs.
     */
    Drupal.behaviors.trackingSettingsSummary = {
        attach: function(context) {
            // Make sure this behavior is processed only if drupalSetSummary is defined.
            if (typeof jQuery.fn.drupalSetSummary == 'undefined') {
                return;
            }

            $('fieldset#edit-page-vis-settings', context).drupalSetSummary(function(context) {
                var $radio = $('input[name="togetherjs_visibility_pages"]:checked', context);
                if ($radio.val() == 0) {
                    if (!$('textarea[name="togetherjs_pages"]', context).val()) {
                        return Drupal.t('Not restricted');
                    }
                    else {
                        return Drupal.t('All pages with exceptions');
                    }
                }
                else {
                    return Drupal.t('Restricted to certain pages');
                }
            });

            $('fieldset#edit-role-vis-settings', context).drupalSetSummary(function(context) {
                var vals = [];
                $('input[type="checkbox"]:checked', context).each(function() {
                    vals.push($.trim($(this).next('label').text()));
                });
                if (!vals.length) {
                    return Drupal.t('Not restricted');
                }
                else if ($('input[name="togetherjs_visibility_roles"]:checked', context).val() == 1) {
                    return Drupal.t('Excepted: @roles', {'@roles': vals.join(', ')});
                }
                else {
                    return vals.join(', ');
                }
            });

            $('fieldset#edit-mobile-vis-settings', context).drupalSetSummary(function(context) {
                var mobile_vals = [];
                if ($('input#edit-togetherjs-block-mobile', context).is(':checked')) {
                    mobile_vals.push(Drupal.t('Mobile is blocked by server'));
                }
                /*  *********  Originally this was going to be used for a javascript based on whether or not the user was using a mobile device.  However, after some thought and testing this is being postponed or possible removed later on.    *******************
                 
                 if ($('input#edit-togetherjs-block-mobile-with-js', context).is(':checked')) {
                 mobile_vals.push(Drupal.t('Mobile is blocked by javascript'));
                 }
                 */
                if (!mobile_vals.length) {
                    return Drupal.t('Mobile is NOT blocked');
                }
                return Drupal.t('@items', {'@items': mobile_vals.join(', ')});
            });

        }
    };

})(jQuery);