<?php

/**
 * @file
 *
 * @author greenSkin
 */
/* * *****************************************************************************
 * Callback Functions, Forms, and Tables
 * **************************************************************************** */

/**
 * Settings form for TogetherJS Collaboration script.
 */
function togetherjs_admin_settings() {
    $form['library'] = array(
      '#type' => 'fieldset',
      '#title' => t('General Library Settings'),
    );

    $location_options = array(
      'external' => t('Externally hosted script'),
      'cached' => t('Locally cache externally hosted script.  (Updated daily)'),
    );

    // If the Libraries API is installed and the TogetherJS script is installed add another option.
    if (module_exists(libraries) && libraries_get_path('togetherjs')) {

        $location_options['libraries_api'] = t('Locally hosted using Libraries API');
    }

    // If the Libraries API is NOT installed but the default_value is set for using the Libraries API.  (Such as the user disabling the module for some reason.)  Then update to use the externally hosted option.
    if (variable_get('togetherjs_library_location', 'external') == 'libraries_api' && (!module_exists(libraries) || !libraries_get_path('togetherjs'))) {
        $location_default = 'external';
    }
    else {
        //  Otherwise just pass the variable along unchanged.
        $location_default = variable_get('togetherjs_library_location', 'external');
    }


    $form['library']['togetherjs_library_location'] = array(
      '#title' => t('TogetherJS Library Location'),
      '#type' => 'radios',
      '#default_value' => $location_default,
      '#options' => $location_options,
      '#required' => TRUE,
      '#description' => t('This determines if you will be using the TogetherJS javascript hosted by <a target="_blank" href="http://www.togetherjs.com">TogetherJS.com</a> , cached locally everyday or uses a  local copy.  In order to be able to use the local option, the <a target="_blank" href="http://drupal.org/project/libraries">Libraries API</a> module must be installed and the <a target="_blank" href="http://www.togetherjs.com">TogetherJS.com</a> script must be downloaded to the "/all/libraries/togetherjs" folder.  PLEASE NOTE: If you are using the externally hosted option, the file will not be aggregated with other javascript files.'),
    );


    $form['library']['togetherjs_ext_location'] = array(
      '#type' => 'textfield',
      '#title' => t('Hosted script location'),
      '#description' => t('If you are using an externally hosted script, what is the URL to be used?  <br> NOTE: The default <a target="_blank" href=\"https://togetherjs.com/togetherjs-min.js\">https://togetherjs.com/togetherjs-min.js</a> should only be used unless you have a good reason to change it.'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#default_value' => variable_get('togetherjs_ext_host', 'https://togetherjs.com/togetherjs-min.js'),
      '#states' => array(
        'visible' => array(
          ':input[name="togetherjs_library_location"]' => array('value' => 'external'),
        ),
      ),
    );


    $form['library']['togetherjs_js_scope'] = array(
      '#type' => 'select',
      '#title' => t('JavaScript scope'),
      '#description' => t('Do you want the script added in the header or footer of the page>'),
      '#options' => array(
        'footer' => t('Footer'),
        'header' => t('Header'),
      ),
      '#default_value' => variable_get('togetherjs_js_scope', 'header'),
    );

    // TogetherJS Parameters.
    $form['parameters_title'] = array(
      '#type' => 'item',
      '#title' => t('TogetherJS Parameters'),
    );
    $form['parameters'] = array(
      '#type' => 'vertical_tabs',
      '#attached' => array(
        'js' => array(drupal_get_path('module', 'togetherjs') . '/js/togetherjs.admin.js'),
      ),
    );

    // Render the TogetherJS Script Options.
    $form['parameters']['togetherjs_configuration'] = array(
      '#type' => 'fieldset',
      '#title' => t('Script Configuration'),
      '#weight' => -15,
    );

    $form['parameters']['togetherjs_configuration']['togetherjs_config_sitename'] = array(
      '#type' => 'textfield',
      '#title' => t('Site Name'),
      '#description' => t('This is the name of your site. It defaults to the title of the page, but often a more abbreviated title is appropriate. This is used in some help text.'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#default_value' => variable_get('togetherjs_config_sitename', variable_get('site_name')),
    );

    $form['parameters']['togetherjs_configuration']['togetherjs_config_toolname'] = array(
      '#type' => 'textfield',
      '#title' => t('Tool Name'),
      '#description' => t('This is the name that you are giving this tool. If you use this then "TogetherJS" won\'t be in the UI. So you might want to use "Collaboration".'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
      '#default_value' => variable_get('togetherjs_config_toolname', 'TogetherJS'),
    );

    // Seperate the more advanced options.
    $form['parameters']['togetherjs_configuration']['advanced'] = array(
      '#type' => 'fieldset',
      '#title' => t('Advanced Options'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_config_hubbase'] = array(
      '#type' => 'textfield',
      '#title' => t('Hub Base'),
      '#description' => t('This is where the hub lives. The hub is a simple server that echoes messages back and forth between clients. It\'s the only real server component of TogetherJS (besides the statically hosted scripts). It\'s also really boring. If you wanted to use a hub besides ours you can override it here. The primary reason would be for privacy; though we do not look at any traffic, by hosting the hub yourself you can be more assured that it is private. You\'ll find that a hub with a valid https certificate is very useful, as mixed http/https is strictly forbidden with WebSockets.  <br> <br>  PLEASE NOTE: Leave this EMPTY if you want to use the default hub that is hosted by the developers of the TogetherJS script.  ONLY enter a different host if you want to use your own hug instead.'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => variable_get('togetherjs_config_hubbase'),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_config_dontshowclicks'] = array(
      '#type' => 'textfield',
      '#title' => t('Don\'t Show Clicks'),
      '#description' => t('This should be set to a jQuery selector or set to true. This will disable the visual click display indicating that a user has clicked on the defined element. For example: "canvas", "h1", "p", etc. Setting to "true" will globally disable all clicks.'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => variable_get('togetherjs_config_dontshowclicks'),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_config_cloneclicks'] = array(
      '#type' => 'textfield',
      '#title' => t('Clone Clicks'),
      '#description' => t('This should be set to a jQuery selector. Whenever someone clicks on an element matching this selector, that click will be repeated (as an actual click) on everyone else\'s browser. This is useful for cases when a click typically doesn\'t do anything, but shows or hides or switches the view of the page. Note that any control that toggles will definitely not work here! If you have tab buttons that show different things you might use ".tab"'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => variable_get('togetherjs_config_cloneclicks'),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_config_enableshortcut'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Shortcut'),
      '#description' => t('If you want to try TogetherJS out on an application, but don\'t want to put up a "Start TogetherJS" button, you can set this to "true" and then an event handler will be put into place that will start TogetherJS when you hit alt-T alt-T (twice in a row!). TogetherJS will still automatically start when someone opens an invitation link.'),
      '#default_value' => variable_get('togetherjs_config_enableshortcut', FALSE),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_config_useminimizedcode'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use Minimized Code'),
      '#description' => t('Typically if you use togetherjs.js you\'ll get the unminimized and uncombined code, with each module loaded lazily. If you use togetherjs-min.js you get the combined code. But if you want to override that more dynamically, you can use this setting.'),
      '#default_value' => variable_get('togetherjs_config_useminimizedcode', FALSE),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_config_findroom'] = array(
      '#type' => 'textfield',
      '#title' => t('Find Room'),
      '#description' => t('To see this in action, check out the examples. This setting assigns people to a room. If you use a single string, this will be the name of the room; for instance:  "my_site_com_users". You can also assign people to a series of rooms with maximum occupancy (what our examples do): {prefix: "my_site_com_users", max: 5}'),
      '#size' => 60,
      '#maxlength' => 128,
      '#default_value' => variable_get('togetherjs_config_findroom'),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_config_autostart'] = array(
      '#type' => 'checkbox',
      '#title' => t('Auto Start'),
      '#description' => t('If "true" then start TogetherJS automatically. Note TogetherJS already starts automatically when a session is continued, so if you just always call TogetherJS() then you might cause TogetherJS to stop. Note you must set this as "true", not using ("autoStart", true) (it must be set when TogetherJS loads). Anyone who autostarts a session will not be considered the session creator.'),
      '#default_value' => variable_get('togetherjs_config_autostart', FALSE),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_suppressjoinconfirmation'] = array(
      '#type' => 'checkbox',
      '#title' => t('Suppress Join Confirmation'),
      '#description' => t('When a person is invited to a session, they\'ll be asked if they want to join in browsing with the other person. Set this to "true" and they won\'t be asked to confirm joining. Useful when combined with findRoom.'),
      '#default_value' => variable_get('togetherjs_suppressjoinconfirmation', FALSE),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_suppressinvite'] = array(
      '#type' => 'checkbox',
      '#title' => t('Suppress Invite'),
      '#description' => t('When a person starts a session, usually a window will pop open with the invite link so they can send it to someone. If this is "true" then that window doesn\'t automatically pop open (but it is still available).'),
      '#default_value' => variable_get('togetherjs_suppressinvite', FALSE),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_config_invitefromroom'] = array(
      '#type' => 'checkbox',
      '#title' => t('Invite From Room'),
      '#description' => t('This adds the ability from the profile menu to invite people who are hanging out in another room (using TogetherJS). This is kind of (but not exactly) how the "Get Help" button works on this site. This is still an experimental feature.'),
      '#default_value' => variable_get('togetherjs_config_invitefromroom', FALSE),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_config_includehashinurl'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include Hash in URL'),
      '#description' => t('When true (default false), TogetherJS treats the entire URL, including the hash, as the identifier of the page; i.e., if you one person is on http://example.com/#view1 and another person is at http://example.com/#view2 then these two people are considered to be at completely different URLs. You\'d want to use this in single-page apps where being at the same base URL doesn\'t mean the two people are looking at the same thing.'),
      '#default_value' => variable_get('togetherjs_config_includehashinurl', FALSE),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_config_disablewebrtc'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable Web RTC'),
      '#description' => t('Disables/removes the button to do audio chat via WebRTC.'),
      '#default_value' => variable_get('togetherjs_config_disablewebrtc', FALSE),
    );
    $form['parameters']['togetherjs_configuration']['advanced']['togetherjs_config_youtube'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable Youtube Video Synchronization'),
      '#description' => t('When enabled, this synchronizes Youtube videos for users in a session tegether.'),
      '#default_value' => variable_get('togetherjs_config_youtube', FALSE),
    );


    // Page specific visibility configurations.
    $php_access = user_access('use PHP for togetherjs visibility');
    $visibility = variable_get('togetherjs_visibility_pages', 0);
    $pages = variable_get('togetherjs_pages', TOGETHERJS_PAGES);

    $form['parameters']['page_vis_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Pages'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => -5,
    );

    if ($visibility == 2 && !$php_access) {
        $form['parameters']['page_vis_settings'] = array();
        $form['parameters']['page_vis_settings']['visibility'] = array('#type' => 'value', '#value' => 2);
        $form['parameters']['page_vis_settings']['pages'] = array('#type' => 'value', '#value' => $pages);
    }
    else {
        $options = array(
          t('Every page except the listed pages'),
          t('The listed pages only'),
        );
        $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')) . " <br> " . t("PLEASE NOTE: Any pages that DO NOT have the <a target=\"_blank\" href=\"http://www.togetherjs.com\">TogetherJS</a> script enabled will prevent the user from being able to collaborate on that page.  If they are currently in a session with another user, they will appear to go offline until they go to another page with the script enabled.");

        if (module_exists('php') && $php_access) {
            $options[] = t('Pages on which this PHP code returns <code>TRUE</code> (experts only)');
            $title = t('Pages or PHP code');
            $description .= ' <br> <br> ' . t('If the PHP option is chosen, enter PHP code between %php. Note that executing incorrect PHP code can break your Drupal site.', array('%php' => '<?php ?>'));
        }
        else {
            $title = t('Pages');
        }
        $form['parameters']['page_vis_settings']['togetherjs_visibility_pages'] = array(
          '#type' => 'radios',
          '#title' => t('Add TogetherJS to specific pages'),
          '#options' => $options,
          '#default_value' => $visibility,
        );
        $form['parameters']['page_vis_settings']['togetherjs_pages'] = array(
          '#type' => 'textarea',
          '#title' => $title,
          '#title_display' => 'invisible',
          '#default_value' => $pages,
          '#description' => $description,
          '#rows' => 10,
        );
    }

    // Render the role overview.
    $form['parameters']['role_vis_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Roles'),
      '#weight' => 0,
    );

    $form['parameters']['role_vis_settings']['togetherjs_visibility_roles'] = array(
      '#type' => 'radios',
      '#title' => t('Add TogetherJS for specific roles'),
      '#options' => array(
        t('Add to the selected roles only'),
        t('Add to every role except the selected ones'),
      ),
      '#default_value' => variable_get('togetherjs_visibility_roles', 0),
    );

    $role_options = array_map('check_plain', user_roles());
    $form['parameters']['role_vis_settings']['togetherjs_roles'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Roles'),
      '#default_value' => variable_get('togetherjs_roles', array()),
      '#options' => $role_options,
      '#description' => t('If none of the roles are selected, all users will be able to use <a target="_blank" href="http://www.togetherjs.com">TogetherJS</a>. If a user has any of the roles checked, that user will be able to use <a target="_blank" href="http://www.togetherjs.com">TogetherJS</a> (or excluded, depending on the setting above).'),
    );

    // Mobile Settings.
    $form['parameters']['mobile_vis_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Mobile'),
      '#weight' => 0,
    );
    $form['parameters']['mobile_vis_settings']['togetherjs_block_mobile'] = array(
      '#type' => 'checkbox',
      '#title' => t('Block mobile devices with server detection'),
      '#description' => t('If you do NOT want mobile devices to be able to use the <a target="_blank" href="http://www.togetherjs.com">TogetherJS.com</a> script you can optionally block them.  When mobile users are blocked every attempt will be made to properly identify their devices and prevent smartphones and tablets from using this script.  <br> <br>  PLEASE NOTE: It is HIGHLY recommended to use the <a target="_blank" href="https://drupal.org/project/browscap">Browscap</a> module when using this feature.  This module will use it automatically when it is available.  <br> <br>  IMPORTANT: This option uses server side detection techniques.  Unfortunately, this can be incompatible with certain caching scenerios such as anonomous users on a site using Varnish or Boost.'),
      '#default_value' => variable_get('togetherjs_prevent_on_mobile', 'FALSE'),
    );

    /*     * ********  Originally this was going to be used for a javascript based test on whether or not the user was using a mobile device.  However, after some thought and testing this is being postponed or possible removed later on.    *******************

      $form['parameters']['mobile_vis_settings']['togetherjs_block_mobile_with_js'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use javascript detection to block mobile devices'),
      '#description' => t('When enabled this will attempt to use a javascript detection technique to determine if the user is on a mobile device.  This can be useful in certain caching scenerios when using say Varnish or Boost for anonomous users.  However it does depend upon the users browser for proper detection.  While this will not work if the user\'s browser has javascript disabled, in that case the TogetherJS script would not work either.  <br> <br> PLEASE NOTE:  This option will only become available if you have the <a href="https://drupal.org/project/libraries">Libraries API</a> enabled and the <a href="http://detectmobilebrowsers.com/download/jquery">DetectMobileBrowsers.com - jQuery Mobile Detection Script</a> download to the "/all/libraries/detectmobilebrowsers" folder.'),
      '#default_value' => variable_get('togetherjs_prevent_on_mobile_with_js', 'FALSE'),
      );

      // If the Libraries API is NOT installed or the DetectMobileBrowsers.com script is NOT found then disable the option and set the default to FALSE.
      if (!module_exists(libraries) || !libraries_get_path('detectmobilebrowsers')) {

      $form['parameters']['mobile_vis_settings']['togetherjs_block_mobile_with_js']['#disabled'] = TRUE;
      $form['parameters']['mobile_vis_settings']['togetherjs_block_mobile_with_js']['#default_value'] = FALSE;

      }
     */


    // Add a custom submit handler to allow for a bit of custom code to be run on configuration updates.
    $form['#submit'][] = 'togetherjs_settings_form_submit';

    return system_settings_form($form);
}

/**
 * Custom page submit handler().
 * This allows for a few custom adjustments before the default submit handler can process the form.
 */
function togetherjs_settings_form_submit($form, &$form_state) {

    // Trim some text values.
    $form_state['values']['togetherjs_pages'] = trim($form_state['values']['togetherjs_pages']);

    // Clear obsolete local cache on save so that updated info is used.
    togetherjs_clear_js_cache();
}
