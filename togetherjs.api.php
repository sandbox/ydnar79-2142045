<?php

/**
 * @file
 * Hooks provided by the core TogetherJS module.
 */

/**
 * Add additional javascript settings for TogetherJS before adding the core TogetherJS script.
 *
 * This hook allows additional javascript settings to be added to the page
 * before the primary TogetherJS script is added.
 * This is done now since the TogetherJS script will load in these variables
 * when it first begins.
 *
 */
function hook_togetherjs_js_settings() {

    // Check where the script should be located in the page.
    $scope = variable_get('togetherjs_js_scope', 'header');

    drupal_add_js('Some custom javascript', array('type' => 'inline', 'scope' => $scope, 'weight' => -1));

    // Or add an external javascript file.
    drupal_add_js(drupal_get_path('module', 'MyAmazingModule') . 'some_special_sauce.js', array('scope' => $scope, 'weight' => -1));

}
