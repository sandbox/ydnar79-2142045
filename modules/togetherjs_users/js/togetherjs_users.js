jQuery(document).ready(function($) {

    /**
     * Check for certain cookies and adjust TogetherJS parameters as needed.
     */

// Drupal does not have the removeCookie function in the included cookie plugin, so create our own function to get rid of the cookies.
    $.TogetherJS_deleteCookie = function(key, options) {
        if ($.cookie(key) === undefined) {
            return false;
        }

        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, {expires: -1}));
        return !$.cookie(key);
    };


    // Check if the current user is NOT logged in.
    if ($("body").hasClass('not-logged-in'))
    {
        // Set username to random name
        if ($.cookie("Drupal.visitor.TogetherJSConfig_getUserName")) {
            $.TogetherJS_deleteCookie("Drupal.visitor.TogetherJSConfig_getUserName");  // Remove cookie.

            var DEFAULT_NICKNAMES = [
                "Friendly Fox",
                "Brilliant Beaver",
                "Observant Owl",
                "Gregarious Giraffe",
                "Wild Wolf",
                "Silent Seal",
                "Wacky Whale",
                "Curious Cat",
                "Intelligent Iguana"
            ];


            TogetherJSConfig_getUserName = function() {

                // Randomly pick a name
                var user_name = DEFAULT_NICKNAMES[Math.floor(Math.random() * DEFAULT_NICKNAMES.length)];

                return user_name;
            }
        }

        // Set user avatar to default
        if ($.cookie("Drupal.visitor.TogetherJSConfig_getUserAvatar")) {
            $.TogetherJS_deleteCookie("Drupal.visitor.TogetherJSConfig_getUserAvatar");  // Remove cookie.

            TogetherJSConfig_getUserAvatar = function() {

                var baseAvatarUrl = "https://togetherjs.com";  // Default TogetherJS hosted site.

                var avatar = baseAvatarUrl + "/togetherjs/images/default-avatar.png";

                return avatar;

            }



        }

        // Set user color to random color
        if ($.cookie("Drupal.visitor.TogetherJSConfig_getUserColor")) {
            $.TogetherJS_deleteCookie("Drupal.visitor.TogetherJSConfig_getUserColor");  // Remove cookie.

            // Set a random color
            TogetherJSConfig_getUserColor = function() {

                color = Math.floor(Math.random() * 0xffffff).toString(16);
                while (color.length < 6) {
                    color = "0" + color;
                }
                color = "#" + color;

                return color;
            }



        }
    }

    if ($("body").hasClass('logged-in'))
    {

        // user is logged in

        // Pass User Name if available
        if ($.cookie("Drupal.visitor.TogetherJSConfig_getUserName")) {
            TogetherJSConfig_getUserName = function() {
                return $.cookie("Drupal.visitor.TogetherJSConfig_getUserName");
            }
//			$.TogetherJS_deleteCookie("Drupal.visitor.TogetherJSConfig_getUserName");  // Remove cookie.
        }

        // Pass User Avatar if available
        if ($.cookie("Drupal.visitor.TogetherJSConfig_getUserAvatar")) {
            TogetherJSConfig_getUserAvatar = function() {
                return decodeURIComponent($.cookie("Drupal.visitor.TogetherJSConfig_getUserAvatar"));
            };
//			$.TogetherJS_deleteCookie("Drupal.visitor.TogetherJSConfig_getUserAvatar");  // Remove cookie.

        }

        // Pass User Profile Color if available
        if ($.cookie("Drupal.visitor.TogetherJSConfig_getUserColor")) {
            TogetherJSConfig_getUserColor = function() {
                return $.cookie("Drupal.visitor.TogetherJSConfig_getUserColor");
            };
//			$.TogetherJS_deleteCookie("Drupal.visitor.TogetherJSConfig_getUserColor");  // Remove cookie.

        }
    }


    // Only run after TogetherJS is active and done running.
    TogetherJS.on("ready", function() {

        // If the user just logged in or out, reset TogetherJS in order for the user profile data to be updated.
        if ($.cookie("Drupal.visitor.TogetherJS_update_user_data") != null) {
            // Turn off TogetherJS
            TogetherJS();

            // Remove cookie
            $.TogetherJS_deleteCookie("Drupal.visitor.TogetherJS_update_user_data");

            setTimeout(function() {
                TogetherJS()
            }, 100);  // Restart TogetherJS after a slight delay.  For some reason it does not want to always restart without using the delay.

        }
    })


}




);

