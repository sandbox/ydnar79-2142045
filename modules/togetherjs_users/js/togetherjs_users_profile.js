jQuery(document).ready(function($) {

    /**
     * This will change the built-in profile area in TogetherJS since Drupal is taking over the functionality.
     */

    TogetherJS.on("ready", function() {
        $("#togetherjs-menu-update-name").remove();

        var TogetherJSUsersBaseURL = Drupal.settings.TogetherJSUsers.TogetherJSUsersBaseURL;
        $("#togetherjs-menu-update-avatar").replaceWith('<div class="togetherjs-menu-item" id="togetherjs-menu-update-profile"><a href="' + TogetherJSUsersBaseURL + '/togetherjs/profile/edit"><img src="https://togetherjs.com/togetherjs/images/btn-menu-change-avatar.png" alt=""></a> <a href="' + TogetherJSUsersBaseURL + '/togetherjs/profile/edit">Update profile</a></div>');

    })

}




);

