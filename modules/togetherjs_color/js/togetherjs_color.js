jQuery(document).ready(function($) {

    /**
     * This will change the built-in profile area in TogetherJS since Drupal is taking over the functionality.
     */

    TogetherJS.on("ready", function() {
        $("#togetherjs-menu-update-name").remove();
        var TogetherJSColorsDockColor = Drupal.settings.TogetherJSColors.TogetherJSColorsDockColor;
        var TogetherJSColorsButtonHoverColor = Drupal.settings.TogetherJSColors.TogetherJSColorsButtonHoverColor;
        var TogetherJSColorsBoxHeaderColor = Drupal.settings.TogetherJSColors.TogetherJSColorsBoxHeaderColor;
        $("#togetherjs-dock").css("background-color", "#" + TogetherJSColorsDockColor);

        $(".togetherjs #togetherjs-buttons .togetherjs-button").hover(
                function() {
                    $(this).css("background-color", "#" + TogetherJSColorsButtonHoverColor);
                },
                function() {
                    $(this).css("background-color", "#" + TogetherJSColorsDockColor);
                }
        )

        $( ".togetherjs .togetherjs-window > header" ).css("background", "#" + TogetherJSColorsBoxHeaderColor);

    })

}




);

