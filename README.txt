This module allows for easy integration of the amazing TogetherJS widget into a website.  You can find out more here:  https://togetherjs.com/

I am not personally affiliated with this project or Mozilla.  I merely wanted to share an easy way of integrating this wonderful project into a Drupal website on a more intricate manner than just plugging in the script on the page and calling it good.

This module is made up of a core module and several sub modules that expand the functionality even further.  Below is a quick run down of what is currently available.

Feel free to expand the functionality even further and share with the Drupal community!

Core TogetherJS Module:  Adds the basic foundation of the system by adding the TogetherJS script to the site and allowing for basic functionality to be adjusted.

TogetherJS Color Module:  Allows for adjustments to be made to the TogetherJS interface through a Drupal admin screen.

TogetherJS Users Module:  Allows for greater integration of the TogetherJS script and Drupal's core user experience.  With these options, the user's image and profile colors can be adjusted through the Drupal user edit page and the TogetherJS interface is changed to show this difference.

Additional sub modules may be coming in the future....